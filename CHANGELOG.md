# 1.0.0
- Initial import of files to repo
- atlast_exporter uses the latest (as of 2022-09-05) version 1.0.3
- ref: https://github.com/czerwonk/atlas_exporter
- ref: https://github.com/czerwonk/atlas_exporter/releases
- ref: https://labs.ripe.net/author/daniel_czerwonk/using-ripe-atlas-measurement-results-in-prometheus/
- ref: https://academy.apnic.net/wp-content/uploads/2021/09/Using-RIPE-Atlas-for-network-diagnostics-APNI
C.pdf
